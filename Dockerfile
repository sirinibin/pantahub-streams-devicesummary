FROM maven:3.5.4-jdk-8-alpine as builder

WORKDIR /build
COPY pom.xml pom.xml
COPY src src

RUN mvn clean package

FROM azul/zulu-openjdk-debian

RUN apt-get update && apt-get install -y curl && apt-get clean && rm -rf /var/lib/apt/lists

COPY --from=builder /build/target/devicesummary-0.1.jar /target/devicesummary.jar

WORKDIR /target

CMD  java -jar devicesummary.jar

