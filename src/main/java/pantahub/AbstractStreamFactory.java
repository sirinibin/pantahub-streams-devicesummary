/*
 * Copyright 2019  Pantacor Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package pantahub;

import java.io.IOException;
import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.apache.avro.generic.GenericRecord;
import org.apache.avro.util.Utf8;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serdes.StringSerde;
import org.apache.kafka.connect.json.JsonSerializer;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Serialized;
import org.bson.BsonDocument;
import org.bson.json.JsonMode;
import org.bson.json.JsonReader;
import org.bson.json.JsonWriterSettings;

public abstract class AbstractStreamFactory {

    protected KTable<String, JsonNode> createJsonChangeTable(final StreamsBuilder builder, final String sourceTopic) {

        final Serde jsonSerde = Serdes.serdeFrom(new JsonSerializer(),
                new org.apache.kafka.connect.json.JsonDeserializer());

        KStream<String, GenericRecord> source = builder.stream(sourceTopic);

        KGroupedStream<String, JsonNode> changeStream = source.mapValues((v) -> {
            BsonDocument doc = null;
            if (v == null) {
                System.out.println("No document in source mapValues??");
                return null;
            }
            if (v.get("after") != null) {
                String afterString = (String) v.get("after");
                doc = BsonDocument.parse(afterString);
            } else if (v.get("patch") != null) {
                String patchString = (String) v.get("patch");
                doc = BsonDocument.parse(patchString);
            }

            if (doc == null) {
                return null;
            }

            ObjectMapper mapper = new ObjectMapper();
            JsonWriterSettings writerSettings = new JsonWriterSettings(JsonMode.EXTENDED, true);
            String json = doc.toJson(writerSettings);

            JsonNode jsonNode = null;
            try {
                JsonReader reader = new JsonReader(json);
                jsonNode = mapper.readTree(json);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            return jsonNode;
        }).groupByKey(Serialized.with(new StringSerde(), jsonSerde));

        KTable<String, JsonNode> changeTable = changeStream.aggregate(() -> null, (aggKey, v2, v1) -> {
            final ObjectNode v1Obj = (ObjectNode) v1;

            if (v2.get("$set") != null) {
                // if we havent seen a full doc yet, we cannot apply a patch event.
                if (v1Obj == null) {
                    // System.err.println("Error processing stream input stream; no full parent
                    // available for patch");
                    return null;
                }

                JsonNode setter = v2.get("$set");

                Iterator<String> i = setter.fieldNames();
                while (i.hasNext()) {
                    String fieldName = i.next();
                    JsonNode v = setter.get(fieldName);
                    v1Obj.put(fieldName, v);
                }

                return (JsonNode) v1Obj;

            } else if (v2.get("$unset") != null) {
                // if we havent seen a full doc yet, we cannot apply a patch event.
                if (v1Obj == null) {
                    // System.err.println("Error processing stream input stream; no full parent
                    // available for patch");
                    return null;
                }

                JsonNode setter = v2.get("$unset");

                Iterator<String> i = setter.fieldNames();
                while (i.hasNext()) {
                    String fieldName = i.next();
                    JsonNode v = setter.get(fieldName);
                    v1Obj.remove(fieldName);
                }

                return (JsonNode) v1Obj;

            } else if (v2 != null) {
                // here v2 is an after or a full doc put
                return v2;
            }

            // otherwise we try to just return v1Obj
            if (v1Obj == null)
                return v1Obj;
            return v1Obj;
        }, Materialized.with(new StringSerde(), jsonSerde));

        return changeTable;
    }

    protected KStream<String, String> createPatchedDocStream(final StreamsBuilder builder, final String sourceTopic) {

        KTable<String,JsonNode> changeTable = createJsonChangeTable(builder, sourceTopic);

        KStream<String,String> docStream = changeTable.toStream().mapValues((v) -> {
            return v == null ? null : v.toPrettyString();
        });

        return docStream;
    }
}
